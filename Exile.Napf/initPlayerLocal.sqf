/**
 * Created with Exile Mod 3DEN Plugin
 * www.exilemod.com
 */
 
if (!hasInterface || isServer) exitWith {};


private _npcs = [
// Tank & APC
["Exile_Trader_Vehicle", [], "Exile_Trader_CommunityCustoms", "AfricanHead_01", [[],[],[],["U_O_V_Soldier_Viper_F",[]],[],["B_ViperHarness_ghex_F",[]],"H_HelmetO_ViperSP_ghex_F","G_Tactical_Clear",[],["","","","","",""]],[16690.5, 13777.8, 28.8377], [0.604671, -0.796475, 0], [0, 0, 1]],
// Air blackMarket
["Exile_Trader_Aircraft", [], "Exile_Trader_Aircraft", "WhiteHead_06", [[],[],[],["U_I_pilotCoveralls",[]],[],[],"H_PilotHelmetHeli_O","rhs_googles_orange",[],["","","","","",""]], [18122.6, 1984.99, 135.644], [-0.0934305, -0.995626, 0], [0, 0, 1]],
// Spec Ops Equipment
["Exile_Trader_SpecialOperations", [], "Exile_Trader_CommunityCustoms2", "WhiteHead_08", [[],[],[],["U_O_V_Soldier_Viper_F",[]],[],["B_ViperHarness_ghex_F",[]],"H_HelmetO_ViperSP_ghex_F","G_Balaclava_lowprofile",[],["","","","","",""]],[2893.15, 6222.45, 69.8185], [0.972549, -0.232699, 0], [0, 0, 1]],
// Dam Trader
["Exile_Trader_WasteDump", [], "Exile_Trader_WasteDump", "GreekHead_A3_07", [[],[],[],["U_I_G_Story_Protagonist_F",[]],["V_Rangemaster_belt",[]],[],"H_MilCap_gry","G_Tactical_Black",[],["","","","","",""]], [10593.2, 5009.83, 104.366], [0.661587, 0.749869, 0], [0, 0, 1]],
["Exile_Trader_Vehicle", [], "Exile_Trader_Vehicle", "WhiteHead_14", [[],[],[],["Exile_Uniform_ExileCustoms",[]],[],[],"H_RacingHelmet_4_F","G_Tactical_Clear",[],["","","","","",""]], [10598.6, 4971.41, 104.968], [-0.999362, 0.0357137, 0], [0, 0, 1]],
["Exile_Trader_VehicleCustoms", [], "Exile_Trader_VehicleCustoms", "Sturrock", [[],[],[],["Exile_Uniform_ExileCustoms",[]],[],[],"","G_Combat",[],["","","","","",""]], [10598.7, 4976.09, 104.968], [-0.994501, 0.104729, 0], [0, 0, 1]],
["Exile_Trader_Equipment", [], "Exile_Trader_Equipment", "WhiteHead_13", [["arifle_MX_GL_Black_F","","","",[],[],""],[],[],["U_BG_Guerrilla_6_1",[]],["V_I_G_resistanceLeader_F",[]],[],"H_Watchcap_khk","G_Aviator",[],["","","","","",""]], [10557, 4987.83, 105.477], [-0.189148, -0.981948, 0], [0, 0, 1]],
["Exile_Trader_Food", [], "Exile_Trader_Food", "GreekHead_A3_06", [[],[],[],["U_C_Poloshirt_blue",[]],[],[],"H_Cap_tan","",[],["","","","","",""]], [10540.8, 4972.7, 105.466], [0.269945, 0.962876, 0], [0, 0, 1]],
["Exile_Trader_Hardware", [], "Exile_Trader_Hardware", "WhiteHead_08", [[],[],[],["U_C_WorkerCoveralls",[]],["V_BandollierB_rgr",[]],["B_UAV_01_backpack_F",[]],"H_Booniehat_khk_hs","G_Tactical_Clear",[],["","","","","",""]], [10544.7, 4972, 105.447], [0.251411, 0.967881, 0], [0, 0, 1]],
["Exile_Trader_Office", [], "Exile_Trader_Office", "AfricanHead_02", [[],[],[],["U_I_G_resistanceLeader_F",[]],["V_Rangemaster_belt",[]],[],"H_Hat_brown","",[],["","","","","",""]], [10543.1, 4989.21, 105.477], [0.481919, -0.876216, 0], [0, 0, 1]],
["Exile_Trader_SpecialOperations", [], "Exile_Trader_SpecialOperations", "WhiteHead_10", [["arifle_MX_Black_F","","","",[],[],""],[],[],["U_B_CTRG_1",[]],["V_PlateCarrierGL_blk",[]],["B_Parachute",[]],"H_HelmetB_light_black","G_Balaclava_lowprofile",[],["","","","","","NVGoggles_OPFOR"]], [10548.9, 4971.24, 105.427], [0, 1, 0], [0, 0, 1]],
["Exile_Trader_Armory", [], "Exile_Trader_Armory", "GreekHead_A3_09", [["srifle_DMR_06_olive_F","","","",[],[],""],[],[],["U_Rangemaster",[]],["V_Rangemaster_belt",[]],[],"H_Cap_headphones","G_Shades_Black",[],["","","","","",""]], [10554.2, 4988.37, 105.477], [-0.133351, -0.991069, 0], [0, 0, 1]],
// Freidorf
["Exile_Trader_WasteDump", [], "Exile_Trader_WasteDump", "AfricanHead_03", [[],[],[],["U_I_G_Story_Protagonist_F",[]],["V_Rangemaster_belt",[]],[],"H_MilCap_gry","G_Combat",[],["","","","","",""]], [8389.73, 17195.7, 38.6771], [0.903022, -0.429593, 0], [0, 0, 1]],
["Exile_Trader_Vehicle", [], "Exile_Trader_Vehicle", "GreekHead_A3_05", [[],[],[],["Exile_Uniform_ExileCustoms",[]],[],[],"H_RacingHelmet_4_F","G_Tactical_Clear",[],["","","","","",""]], [8411.44, 17210.9, 37.9156], [-0.378573, 0.925571, 0], [0, 0, 1]],
["Exile_Trader_VehicleCustoms", [], "Exile_Trader_VehicleCustoms", "Sturrock", [[],[],[],["Exile_Uniform_ExileCustoms",[]],[],[],"","G_Combat",[],["","","","","",""]], [8415.69, 17213.1, 37.6768], [-0.313669, 0.949532, 0], [0, 0, 1]],
["Exile_Trader_Equipment", [], "Exile_Trader_Equipment", "WhiteHead_17", [["arifle_MX_GL_Black_F","","","",[],[],""],[],[],["U_BG_Guerrilla_6_1",[]],["V_I_G_resistanceLeader_F",[]],[],"H_Watchcap_khk","",[],["","","","","",""]], [8428.61, 17162.9, 39.5926], [-0.912698, -0.408635, 0], [0, 0, 1]],
["Exile_Trader_Food", [], "Exile_Trader_Food", "WhiteHead_06", [[],[],[],["U_C_Poloshirt_blue",[]],[],[],"H_Cap_tan","G_Tactical_Clear",[],["","","","","",""]], [8401.35, 17158, 39.5929], [0.935827, 0.35246, 0], [0, 0, 1]],
["Exile_Trader_Hardware", [], "Exile_Trader_Hardware", "WhiteHead_07", [[],[],[],["U_C_WorkerCoveralls",[]],["V_BandollierB_rgr",[]],["B_UAV_01_backpack_F",[]],"H_Booniehat_khk_hs","G_Tactical_Clear",[],["","","","","",""]], [8403.08, 17154.5, 39.5926], [0.928888, 0.37036, 0], [0, 0, 1]],
["Exile_Trader_Office", [], "Exile_Trader_Office", "WhiteHead_20", [[],[],[],["U_I_G_resistanceLeader_F",[]],["V_Rangemaster_belt",[]],[],"H_Hat_brown","G_Tactical_Clear",[],["","","","","",""]], [8399.14, 17175.8, 39.5926], [0.481919, -0.876216, 0], [0, 0, 1]],
["Exile_Trader_SpecialOperations", [], "Exile_Trader_SpecialOperations", "Barklem", [["arifle_MX_Black_F","","","",[],[],""],[],[],["U_B_CTRG_1",[]],["V_PlateCarrierGL_blk",[]],["B_Parachute",[]],"H_HelmetB_light_black","G_Balaclava_lowprofile",[],["","","","","","NVGoggles_OPFOR"]], [8404.92, 17150.7, 39.5931], [0.80594, 0.591997, 0], [0, 0, 1]],
["Exile_Trader_Armory", [], "Exile_Trader_Armory", "AfricanHead_02", [["srifle_DMR_06_olive_F","","","",[],[],""],[],[],["U_Rangemaster",[]],["V_Rangemaster_belt",[]],[],"H_Cap_headphones","G_Shades_Black",[],["","","","","",""]], [8425.63, 17169.6, 39.5926], [-0.881493, -0.472197, 0], [0, 0, 1]],
["Exile_Trader_Boat", [], "Exile_Trader_Boat", "GreekHead_A3_08", [[],[],[],["U_OrestesBody",[]],[],[],"H_Cap_surfer","",[],["","","","","",""]], [8130.6, 17359, 1.90298], [0.0588707, -0.998266, 0], [0, 0, 1]],
// Giswil
["Exile_Trader_WasteDump", [], "Exile_Trader_WasteDump", "AfricanHead_01", [[],[],[],["U_I_G_Story_Protagonist_F",[]],["V_Rangemaster_belt",[]],[],"H_MilCap_gry","G_Combat",[],["","","","","",""]], [16833.1, 5136.84, 24.6891], [-0.987392, -0.158296, 0], [0, 0, 1]],
["Exile_Trader_Vehicle", [], "Exile_Trader_Vehicle", "WhiteHead_01", [[],[],[],["Exile_Uniform_ExileCustoms",[]],[],[],"H_RacingHelmet_4_F","G_Tactical_Clear",[],["","","","","",""]], [16815.8, 5086.04, 21.6633], [0.99251, 0.122159, 0], [0, 0, 1]],
["Exile_Trader_VehicleCustoms", [], "Exile_Trader_VehicleCustoms", "WhiteHead_01", [[],[],[],["Exile_Uniform_ExileCustoms",[]],[],[],"","G_Combat",[],["","","","","",""]], [16815.1, 5095.84, 21.8338], [0.999882, -0.0153828, 0], [0, 0, 1]],
["Exile_Trader_Food", [], "Exile_Trader_Food", "WhiteHead_02", [[],[],[],["U_C_Poloshirt_blue",[]],[],[],"H_Cap_tan","G_Combat",[],["","","","","",""]], [16830, 5179.78, 24.6719], [0.649884, -0.760034, 0], [0, 0, 1]],
["Exile_Trader_Office", [], "Exile_Trader_Office", "GreekHead_A3_06", [[],[],[],["U_I_G_resistanceLeader_F",[]],["V_Rangemaster_belt",[]],[],"H_Hat_brown","G_Combat",[],["","","","","",""]], [16825.1, 5178.76, 24.6719], [0.786636, -0.617418, 0], [0, 0, 1]],
["Exile_Trader_Equipment", [], "Exile_Trader_Equipment", "Barklem", [["arifle_MX_GL_Black_F","","","",[],[],""],[],[],["U_BG_Guerrilla_6_1",[]],["V_I_G_resistanceLeader_F",[]],[],"H_Watchcap_khk","G_Tactical_Clear",[],["","","","","",""]], [16823.5, 5177.01, 24.6719], [0.952534, -0.304432, 0], [0, 0, 1]],
["Exile_Trader_Hardware", [], "Exile_Trader_Hardware", "WhiteHead_16", [[],[],[],["U_C_WorkerCoveralls",[]],["V_BandollierB_rgr",[]],["B_UAV_01_backpack_F",[]],"H_Booniehat_khk_hs","G_Combat",[],["","","","","",""]], [16824.5, 5173.21, 24.6719], [0.604671, 0.796475, 0], [0, 0, 1]],
["Exile_Trader_SpecialOperations", [], "Exile_Trader_SpecialOperations", "WhiteHead_18", [["arifle_MX_Black_F","","","",[],[],""],[],[],["U_B_CTRG_1",[]],["V_PlateCarrierGL_blk",[]],["B_Parachute",[]],"H_HelmetB_light_black","G_Balaclava_lowprofile",[],["","","","","","NVGoggles_OPFOR"]], [16836.1, 5173.6, 24.6719], [-0.428653, -0.903469, 0], [0, 0, 1]],
["Exile_Trader_Armory", [], "Exile_Trader_Armory", "WhiteHead_04", [["srifle_DMR_06_olive_F","","","",[],[],""],[],[],["U_Rangemaster",[]],["V_Rangemaster_belt",[]],[],"H_Cap_headphones","G_Shades_Black",[],["","","","","",""]], [16836.9, 5169.66, 24.6719], [-0.972284, 0.233802, 0], [0, 0, 1]],
["Exile_Trader_Boat", [], "Exile_Trader_Boat", "WhiteHead_17", [[],[],[],["U_OrestesBody",[]],[],[],"H_Cap_surfer","G_Combat",[],["","","","","",""]], [16835.2, 5278.85, 1.54008], [0.974193, -0.225716, 0], [0, 0, 1]],
// Blackmarket Boat
["Exile_Trader_Boat", [], "Exile_Trader_CommunityCustoms3", "AfricanHead_01", [[],[],[],["U_O_V_Soldier_Viper_F",[]],[],["B_ViperHarness_ghex_F",[]],"H_HelmetO_ViperSP_ghex_F","G_Tactical_Clear",[],["","","","","",""]],[1555.32, 19375.3, 3.65131], [0.998698, -0.051021, 0], [0, 0, 1]],
// Random Boat Traders
["Exile_Trader_Boat", [], "Exile_Trader_Boat", "WhiteHead_03", [[],[],[],["U_OrestesBody",[]],[],[],"H_Cap_surfer","G_Tactical_Clear",[],["","","","","",""]], [6075.61, 10744.1, 6.00405], [-0.00289711, -0.999996, 0], [0, 0, 1]],
["Exile_Trader_Boat", [], "Exile_Trader_Boat", "WhiteHead_13", [[],[],[],["U_OrestesBody",[]],[],[],"H_Cap_surfer","rhs_googles_yellow",[],["","","","","",""]], [14481.1, 2700.71, 5.01619], [-0.0956148, 0.995418, 0], [0, 0, 1]],
// Air customs
["Exile_Trader_AircraftCustoms", [], "Exile_Trader_AircraftCustoms", "WhiteHead_07", [[],[],[],["Exile_Uniform_ExileCustoms",[]],["V_RebreatherB",[]],[],"H_PilotHelmetFighter_B","G_Combat",[],["","","","","",""]], [8420.07, 17215.1, 37.4436], [-0.498821, 0.866705, 0], [0, 0, 1]],
["Exile_Trader_AircraftCustoms", [], "Exile_Trader_AircraftCustoms", "WhiteHead_03", [[],[],[],["Exile_Uniform_ExileCustoms",[]],["V_RebreatherB",[]],[],"H_PilotHelmetFighter_B","rhs_googles_yellow",[],["","","","","",""]], [16813.9, 5080.74, 21.6293], [0.115399, -0.993319, 0], [0, 0, 1]],
["Exile_Trader_AircraftCustoms", [], "Exile_Trader_AircraftCustoms", "WhiteHead_06", [[],[],[],["Exile_Uniform_ExileCustoms",[]],["V_RebreatherB",[]],[],"H_PilotHelmetFighter_B","rhs_googles_orange",[],["","","","","",""]], [10598.7, 4992.17, 104.927], [-0.993195, -0.116462, 0], [0, 0, 1]]
];

{
    private _logic = "Logic" createVehicleLocal [0, 0, 0];
    private _trader = (_x select 0) createVehicleLocal [0, 0, 0];
    private _animations = _x select 1;
    
    _logic setPosWorld (_x select 5);
    _logic setVectorDirAndUp [_x select 6, _x select 7];
    
    _trader setVariable ["BIS_enableRandomization", false];
    _trader setVariable ["BIS_fnc_animalBehaviour_disable", true];
    _trader setVariable ["ExileAnimations", _animations];
    _trader setVariable ["ExileTraderType", _x select 2];
    _trader disableAI "ANIM";
    _trader disableAI "MOVE";
    _trader disableAI "FSM";
    _trader disableAI "AUTOTARGET";
    _trader disableAI "TARGET";
    _trader disableAI "CHECKVISIBLE";
    _trader allowDamage false;
    _trader setFace (_x select 3);
    _trader setUnitLoadOut (_x select 4);
    _trader setPosWorld (_x select 5);
    _trader setVectorDirAndUp [_x select 6, _x select 7];
    _trader reveal _logic;
    _trader attachTo [_logic, [0, 0, 0]];
    _trader switchMove (_animations select 0);
    _trader addEventHandler ["AnimDone", {_this call ExileClient_object_trader_event_onAnimationDone}];
}
forEach _npcs;


